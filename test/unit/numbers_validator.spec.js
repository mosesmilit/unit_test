const { expect } = require('chai');
const NumbersValidator = require('../../app/numbers_validator');

describe('Tests NumbersValidator Class', () => {
  const evenNum = 4;
  const oddNum = 5;
  const stringVar = '4';
  const arrOfNums = [1, 2, 3, 4];
  const mixedArr = [1, 2, '3', 4];
  const emptyArr = [];
  let validator;

  beforeEach(() => {
    validator = new NumbersValidator();
  });
  afterEach(() => {
    validator = null;
  });

  describe('Tests isNumberEven method', () => {
    it('Should return true if called with an even number.', () => {
      const validationResults = validator.isNumberEven(evenNum);
      expect(validationResults).to.be.equal(true);
    });

    it('Should return false if called with an odd number.', () => {
      const validationResults = validator.isNumberEven(oddNum);
      expect(validationResults).to.be.equal(false);
    });

    it('Should throw an error if called with non number type.', () => {
      expect(() => validator.isNumberEven(stringVar)).to.throw(
        `[${stringVar}] is not of type "Number" it is of type "${typeof stringVar}"`,
      );
    });
  });

  describe('Tests getEvenNumbersFromArray method', () => {
    it('Should return Array of even numbers if called with array of numbers.', () => {
      const evenNums = arrOfNums.filter((num) => num % 2 === 0);

      const validationResults = validator.getEvenNumbersFromArray(arrOfNums);
      expect(validationResults).to.have.same.members(evenNums);
    });

    it('Should throw an error if called with a non array type argument.', () => {
      expect(() => validator.getEvenNumbersFromArray(stringVar)).to.throw(
        `[${stringVar}] is not an array of "Numbers"`,
      );
    });

    it('Should throw an error if called with array which contains non number type item.', () => {
      expect(() => validator.getEvenNumbersFromArray(mixedArr)).to.throw(
        `[${mixedArr}] is not an array of "Numbers"`,
      );
    });

    it('Should throw an error if called with an empty array.', () => {
      expect(() => validator.getEvenNumbersFromArray(emptyArr)).to.throw(
        `This array: [${mixedArr}], is empty.`,
      );
    });
  });

  describe('Tests isAllNumbers method', () => {
    it('Should return true if all items of provided array are numbers.', () => {
      const validationResults = validator.isAllNumbers(arrOfNums);
      expect(validationResults).to.be.equal(true);
    });

    it('Should return false if provided array contains non number type item.', () => {
      const validationResults = validator.isAllNumbers(mixedArr);
      expect(validationResults).to.be.equal(false);
    });

    it('Should throw an error if called with a non array type argument.', () => {
      expect(() => validator.isAllNumbers(stringVar)).to.throw(`[${stringVar}] is not an array`);
    });

    it('Should throw an error if called with an empty array.', () => {
      expect(() => validator.isAllNumbers(emptyArr)).to.throw(
        `This array: [${mixedArr}], is empty.`,
      );
    });
  });

  describe('Tests isInteger method', () => {
    it('Should return true if called with a number.', () => {
      const validationResults = validator.isInteger(evenNum);
      expect(validationResults).to.be.equal(true);
    });

    it('Should throw an error when called with a non array type argument.', () => {
      expect(() => validator.isInteger(stringVar)).to.throw(`[${stringVar}] is not a number`);
    });
  });
});
